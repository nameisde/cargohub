import 'package:cargohub/src/data/repository/repository.dart';
import 'package:cargohub/src/logic/authentication/authentication.dart';
import 'package:cargohub/src/presentation/router/app_router.dart';
import 'package:cargohub/src/presentation/ui/auth/login_page.dart';
import 'package:cargohub/src/presentation/ui/main/home_page.dart';
import 'package:cargohub/src/presentation/ui/splash/splash_page.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class App extends StatelessWidget {
  const App({
    Key? key,
    required this.appRouter,
    required this.connectivity,
    required this.authenticationRepository,
    required this.authenticationBloc,
  }) : super(key: key);

  final AppRouter appRouter;
  final Connectivity connectivity;
  final AuthenticationRepository authenticationRepository;
  final AuthenticationBloc authenticationBloc;

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<AuthenticationBloc>(
          create: (context) => AuthenticationBloc(
            authenticationRepository,
          ),
        ),
      ],
      child: MaterialApp(
        title: 'Cargo Hub',
        debugShowCheckedModeBanner: false,
        theme: ThemeData(primarySwatch: Colors.green),
        onGenerateRoute: appRouter.onGenerateRoute,
        home: BlocBuilder<AuthenticationBloc, AuthenticationState>(
          builder: (context, state) {
            if (state is AuthenticationAuthenticated) {
              return const HomePage();
            } else if (state is AuthenticationUnauthenticated) {
              return LoginPage(
                  authenticationRepository: authenticationRepository,
                  authenticationBloc: authenticationBloc);
            } else if (state is AuthenticationLoading) {
              return const Scaffold(
                body: Center(
                  child: CircularProgressIndicator(),
                ),
              );
            } else if (state is AuthenticationFailure) {
              ScaffoldMessenger.of(context)
                ..hideCurrentSnackBar()
                ..showSnackBar(
                  const SnackBar(
                    content: Text('Kesalahan autentikasi'),
                  ),
                );
            } else if (state is AuthenticationInitial) {
              return SplashPage(
                authenticationRepository: AuthenticationRepository(),
              );
            }
            return const Center(
              child: CircularProgressIndicator(),
            );
          },
        ),
      ),
    );
  }
}
