import 'package:dio/dio.dart';

class DioExceptions implements Exception {
  DioExceptions.fromDioError(DioError dioError) {
    switch (dioError.type) {
      case DioErrorType.cancel:
        message = 'Permintaan ke server API dibatalkan';
        break;
      case DioErrorType.connectTimeout:
        message = 'Batas waktu koneksi dengan server API telah habis';
        break;
      case DioErrorType.other:
        message = 'Koneksi ke server API gagal karena koneksi internet';
        break;
      case DioErrorType.receiveTimeout:
        message = 'Terima batas waktu telah habis dengan server API';
        break;
      case DioErrorType.response:
        message = _handleError(
            dioError.response!.statusCode, dioError.response!.data);
        break;
      case DioErrorType.sendTimeout:
        message = 'Kirim batas waktu sehubungan dengan server API';
        break;
      default:
        message = 'Oops ada yang tidak beres';
        break;
    }
  }

  String? message;

  String _handleError(int? statusCode, dynamic error) {
    switch (statusCode) {
      case 400:
        return 'Server tak bisa memenuhi permintaan';
      case 404:
        return '${error['message']}';
      case 500:
        return 'Kesalahan pada server dan permintaan tidak dapat diselesaikan';
      case 503:
        return 'Server tidak tersedia untuk menangani permintaan';
      default:
        return 'Oops ada yang tidak beres';
    }
  }

  @override
  String toString() => message!;
}
