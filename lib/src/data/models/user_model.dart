import 'package:equatable/equatable.dart';

class UserModel extends Equatable {
  const UserModel({required this.username, required this.password});

  final String username;
  final String password;

  @override
  List<Object> get props => [username, password];
}
