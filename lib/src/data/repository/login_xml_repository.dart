class LoginXMLRepository {
  LoginXMLRepository({
    this.username,
    this.password,
  });

  final String? username;
  final String? password;

  String loginxml() {
    return '''<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:adin="http://3e.pl/ADInterface">
   <soapenv:Header/>
   <soapenv:Body>
      <adin:queryData>
         <adin:ModelCRUDRequest>
            <adin:ModelCRUD>
               <adin:serviceType>API-LOGIN</adin:serviceType>
               <adin:Filter>value = '$username'</adin:Filter>
               <adin:DataRow>
              <adin:field column="IsActive">
                     <adin:val>Y</adin:val>
                  </adin:field>
               </adin:DataRow>
            </adin:ModelCRUD>
            <adin:ADLoginRequest>
               <adin:user>$username</adin:user>
               <adin:pass>$password</adin:pass>
               <adin:lang>192</adin:lang>
               <adin:ClientID>1000000</adin:ClientID>
               <adin:RoleID>1000002</adin:RoleID>
               <adin:OrgID>1000000</adin:OrgID>
               <adin:WarehouseID>1000000</adin:WarehouseID>
            </adin:ADLoginRequest>
         </adin:ModelCRUDRequest>
      </adin:queryData>
   </soapenv:Body>
</soapenv:Envelope>''';
  }
}
