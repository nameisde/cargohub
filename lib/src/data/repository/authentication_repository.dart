import 'dart:async';

import 'package:cargohub/src/data/constant/authentication_status.dart';
import 'package:cargohub/src/data/models/user_model.dart';
import 'package:cargohub/src/data/repository/login_xml_repository.dart';
import 'package:cargohub/src/data/services/authentication_helper.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

class AuthenticationRepository {
  final _streamController = StreamController<AuthenticationStatus>();
  final _authenticationHelper = AuthenticationHelper();

  Stream<AuthenticationStatus> get status async* {
    await Future<void>.delayed(const Duration(seconds: 1));
    yield AuthenticationStatus.unauthenticated;
    yield* _streamController.stream;
  }

  Future loginAPI(LoginXMLRepository body) async {
    _streamController.add(AuthenticationStatus.authenticated);
    final result = await _authenticationHelper.authlogin(body);
    return result;
  }

  void logOut() {
    _streamController.add(AuthenticationStatus.unauthenticated);
  }

  void dispose() => _streamController.close();

  final FlutterSecureStorage storage = const FlutterSecureStorage();

  Future<bool> hasToken() async {
    final value = await storage.read(key: 'SECURE_STORAGE_TOKEN_AUTH');
    if (value != null) {
      return true;
    } else {
      return false;
    }
  }

  Future<void> persistToken(String token) async {
    await storage.write(key: 'SECURE_STORAGE_TOKEN_AUTH', value: token);
  }

  Future<void> deleteToken() async {
    await storage.delete(key: 'SECURE_STORAGE_TOKEN_AUTH');
  }
}
