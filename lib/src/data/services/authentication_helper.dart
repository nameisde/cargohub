import 'dart:async';
import 'package:cargohub/src/data/dio/exception.dart';
import 'package:cargohub/src/data/dio/interceptor.dart';
import 'package:cargohub/src/data/repository/login_xml_repository.dart';
import 'package:dio/dio.dart';

class AuthenticationHelper {
  static Dio _dio = Dio();

  static const baseUrl = 'https://api-cargo.karyakoe.id';

  static Dio getDio() {
    final options = BaseOptions(receiveTimeout: 5000, connectTimeout: 5000);
    _dio = Dio(options);
    _dio.interceptors.add(Logging());
    return _dio;
  }

  Future post(dynamic data) async {
    try {
      final Response response = await getDio()
          .post(baseUrl, data: data)
          .timeout(const Duration(seconds: 30));
      return response.data;
    } on DioError catch (_) {
      final errorMessage = DioExceptions.fromDioError(_).toString();
      return errorMessage;
    } on TimeoutException catch (_) {
      return 'Gagal menyambung ke server';
    } catch (_) {
      return 'Terjadi kesalahan saat menyambung ke server';
    }
  }

  Future authlogin(LoginXMLRepository body) async {
    final response = await post(body.loginxml());
    return response.toString();
  }
}
