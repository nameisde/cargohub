import 'package:cargohub/src/presentation/utils/app_color.dart';
import 'package:cargohub/src/presentation/utils/text_styles.dart';
import 'package:flutter/material.dart';

class CargoHubButton extends StatelessWidget {
  const CargoHubButton({
    Key? key,
    required this.title,
    this.disabled = false,
    this.busy = false,
    this.borderRadius = false,
    this.onTap,
    this.prefix,
  })  : outline = false,
        super(key: key);

  const CargoHubButton.outline({
    Key? key,
    required this.title,
    this.onTap,
    this.prefix,
  })  : disabled = false,
        busy = false,
        borderRadius = false,
        outline = true,
        super(key: key);

  final String title;
  final bool disabled;
  final bool busy;
  final Function()? onTap;
  final bool outline;
  final Widget? prefix;
  final bool borderRadius;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: AnimatedContainer(
        duration: const Duration(milliseconds: 350),
        width: double.infinity,
        height: 50,
        alignment: Alignment.center,
        decoration: !outline
            ? BoxDecoration(
                color: !disabled ? primaryColor : mediumGrayColor,
                borderRadius: !borderRadius
                    ? BorderRadius.circular(0)
                    : BorderRadius.circular(8),
              )
            : BoxDecoration(
                color: Colors.transparent,
                borderRadius: BorderRadius.circular(8),
                border: Border.all(color: primaryColor),
              ),
        child: !busy
            ? Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  if (prefix != null) prefix!,
                  if (prefix != null) const SizedBox(width: 5),
                  Text(
                    title,
                    style: medium400.copyWith(
                      fontWeight: !outline ? bold : regular,
                      color: !outline ? whiteColor : primaryColor,
                    ),
                  )
                ],
              )
            : const CircularProgressIndicator(
                strokeWidth: 6,
                valueColor: AlwaysStoppedAnimation(
                  Colors.white,
                ),
              ),
      ),
    );
  }
}
