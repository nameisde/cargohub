import 'package:cargohub/src/presentation/utils/app_color.dart';
import 'package:flutter/material.dart';

class CargoHubInputField extends StatelessWidget {
  CargoHubInputField({
    Key? key,
    required this.controller,
    this.hintText = '',
    this.prefix,
    this.suffix,
    this.suffixTapped,
    this.obscureText = false,
    this.focusNode,
    this.keyboardType,
    this.enabled,
    this.validator,
    this.onChanged,
  }) : super(key: key);

  final TextEditingController controller;
  final String hintText;
  final Widget? prefix;
  final Widget? suffix;
  final bool obscureText;
  final Function()? suffixTapped;
  final FocusNode? focusNode;
  final TextInputType? keyboardType;
  final bool? enabled;
  final String? Function(String?)? validator;
  final void Function(String)? onChanged;

  final circularBorder =
      OutlineInputBorder(borderRadius: BorderRadius.circular(8));

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      validator: validator,
      controller: controller,
      focusNode: focusNode,
      obscureText: obscureText,
      keyboardType: keyboardType,
      enabled: enabled,
      onChanged: onChanged,
      style: const TextStyle(height: 1),
      decoration: InputDecoration(
        hintText: hintText,
        hintStyle: TextStyle(color: darkGrayColor.withOpacity(0.25)),
        contentPadding:
            const EdgeInsets.symmetric(vertical: 15, horizontal: 20),
        filled: true,
        fillColor: veryLightGrayColor,
        prefixIcon: prefix,
        suffixIcon: suffix != null
            ? GestureDetector(
                onTap: suffixTapped,
                child: suffix,
              )
            : null,
        border: circularBorder.copyWith(
          borderSide: const BorderSide(color: lightGrayColor),
        ),
        errorBorder: circularBorder.copyWith(
          borderSide: const BorderSide(color: Colors.red),
        ),
        focusedBorder: circularBorder.copyWith(
          borderSide: const BorderSide(color: primaryColor),
        ),
        enabledBorder: circularBorder.copyWith(
          borderSide: const BorderSide(color: lightGrayColor),
        ),
      ),
    );
  }
}
