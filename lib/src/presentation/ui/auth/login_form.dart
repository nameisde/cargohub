import 'package:cargohub/src/data/repository/authentication_repository.dart';
import 'package:cargohub/src/logic/login/login.dart';
import 'package:cargohub/src/presentation/utils/text_styles.dart';
import 'package:cargohub/src/presentation/utils/ui_helpers.dart';
import 'package:cargohub/src/presentation/utils/utils.dart';
import 'package:cargohub/src/presentation/widgets/button.dart';
import 'package:cargohub/src/presentation/widgets/input_field.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:wc_form_validators/wc_form_validators.dart';

class LoginForm extends StatefulWidget {
  const LoginForm({
    Key? key,
    required this.authenticationRepository,
  }) : super(key: key);

  final AuthenticationRepository authenticationRepository;

  @override
  _LoginFormState createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  bool _obscureText = true;
  bool _isSubmit = false;

  final TextEditingController _usernameController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  final _formKey = GlobalKey<FormState>();

  late LoginBloc _loginBloc;

  @override
  void initState() {
    _loginBloc = context.read<LoginBloc>();
    _usernameController.addListener(_onUsernameChanged);
    _passwordController.addListener(_onPasswordChanged);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<LoginBloc, LoginState>(
      listener: (context, state) {
        if (state is LoginFailure) {
          setState(() => _isSubmit = false);
          ScaffoldMessenger.of(context)
            ..hideCurrentSnackBar()
            ..showSnackBar(
              SnackBar(
                backgroundColor: failedColor,
                // content: Text('Gagal login, silahkan coba lagi...'),
                content: Text(state.error),
              ),
            );
        }
        if (state is LoginSubmiting) {
          setState(() => _isSubmit = true);
        }
      },
      child: BlocBuilder<LoginBloc, LoginState>(
        bloc: _loginBloc,
        builder: (context, state) {
          return Form(
            key: _formKey,
            child: ListView(
              children: [
                const Text(
                  'Selamat datang',
                  style: extraBold900,
                ),
                verticalSpaceSmall,
                const Text(
                  'Silahkan masukkan Username\ndan Password Anda',
                  style: medium300,
                ),
                verticalSpaceLarge,
                const Text(
                  'Username',
                  style: medium400,
                ),
                verticalSpaceSmall,
                _usernameInput(),
                verticalSpaceMedium,
                const Text(
                  'Password',
                  style: medium400,
                ),
                verticalSpaceSmall,
                _passwordInput(),
                verticalSpaceLarge,
                _loginButton(),
              ],
            ),
          );
        },
      ),
    );
  }

  Widget _usernameInput() {
    return CargoHubInputField(
      controller: _usernameController,
      hintText: 'Masukkan Username',
      keyboardType: TextInputType.name,
      validator: Validators.required(
        'Username tidak boleh kosong',
      ),
    );
  }

  Widget _passwordInput() {
    return CargoHubInputField(
      controller: _passwordController,
      hintText: 'Masukkan Password',
      obscureText: _obscureText,
      keyboardType: TextInputType.visiblePassword,
      validator: Validators.required(
        'Password tidak boleh kosong',
      ),
      suffix: Icon(
        _obscureText ? Icons.visibility_off : Icons.visibility,
      ),
      suffixTapped: () {
        setState(() {
          _obscureText = !_obscureText;
        });
      },
    );
  }

  Widget _loginButton() {
    return CargoHubButton(
      title: 'Sign in',
      busy: _isSubmit,
      onTap: () {
        if (_formKey.currentState!.validate()) {
          try {
            _onSubmitted();
          } catch (e) {
            ScaffoldMessenger.of(context).showSnackBar(
              SnackBar(
                content: Text(e.toString()),
                backgroundColor: Colors.red,
                duration: const Duration(seconds: 3),
              ),
            );
          }
        }
      },
    );
  }

  @override
  void dispose() {
    _usernameController.dispose();
    _passwordController.dispose();
    super.dispose();
  }

  void _onUsernameChanged() {
    _loginBloc.add(
      UsernameChanged(
        username: _usernameController.text,
      ),
    );
  }

  void _onPasswordChanged() {
    _loginBloc.add(
      PasswordChanged(
        password: _passwordController.text,
      ),
    );
  }

  void _onSubmitted() {
    _loginBloc.add(Submitted(
      username: _usernameController.text,
      password: _passwordController.text,
    ));
  }
}
