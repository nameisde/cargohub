import 'package:cargohub/src/data/repository/authentication_repository.dart';
import 'package:cargohub/src/logic/authentication/authentication_bloc.dart';
import 'package:cargohub/src/logic/login/login.dart';

import 'package:cargohub/src/presentation/ui/auth/login_form.dart';
import 'package:cargohub/src/presentation/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class LoginPage extends StatelessWidget {
  const LoginPage({
    Key? key,
    required this.authenticationRepository,
    required this.authenticationBloc,
  }) : super(key: key);

  final AuthenticationRepository authenticationRepository;
  final AuthenticationBloc authenticationBloc;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: SafeArea(
        child: Container(
          padding: const EdgeInsets.only(
            right: marginMedium,
            left: marginMedium,
            top: marginSuperLarge,
          ),
          child: BlocProvider<LoginBloc>(
            create: (context) => LoginBloc(
              authenticationRepository: authenticationRepository,
              authenticationBloc: authenticationBloc,
            ),
            child: LoginForm(
              authenticationRepository: authenticationRepository,
            ),
          ),
        ),
      ),
    );
  }
}
