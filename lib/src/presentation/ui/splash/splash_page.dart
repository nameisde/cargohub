import 'package:cargohub/src/data/repository/repository.dart';
import 'package:cargohub/src/logic/authentication/authentication.dart';
import 'package:cargohub/src/logic/splash/splash.dart';
import 'package:cargohub/src/presentation/ui/auth/login_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class SplashPage extends StatefulWidget {
  const SplashPage({
    Key? key,
    required this.authenticationRepository,
  }) : super(key: key);

  final AuthenticationRepository authenticationRepository;
  @override
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  final SplashBloc splashBloc = SplashBloc();

  @override
  void initState() {
    splashBloc.add(SetSplash());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocProvider<SplashBloc>(
        create: (_) => splashBloc,
        child: BlocBuilder<SplashBloc, SplashState>(
          builder: (context, state) {
            if ((state is SplashInitial) || (state is SplashLoading)) {
              return _buildSplashWidget();
            } else if (state is SplashLoaded) {
              return LoginPage(
                authenticationRepository: AuthenticationRepository(),
                authenticationBloc: AuthenticationBloc(
                  AuthenticationRepository(),
                ),
              );
            }
            return Container();
          },
        ),
      ),
    );
  }
}

Widget _buildSplashWidget() {
  return const Center(
    child: Text('Logo Splash'),
  );
}
