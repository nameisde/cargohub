import 'package:cargohub/src/data/repository/repository.dart';
import 'package:cargohub/src/logic/authentication/authentication.dart';
import 'package:cargohub/src/presentation/ui/auth/login_page.dart';
import 'package:cargohub/src/presentation/ui/main/home_page.dart';
import 'package:cargohub/src/presentation/ui/splash/splash_page.dart';
import 'package:flutter/material.dart';

class AppRouter {
  Route? onGenerateRoute(RouteSettings settings) {
    switch (settings.name) {
      case '/':
        return MaterialPageRoute<void>(
          builder: (_) => SplashPage(
            authenticationRepository: AuthenticationRepository(),
          ),
        );

      case '/login':
        return MaterialPageRoute<void>(
          builder: (_) => LoginPage(
            authenticationRepository: AuthenticationRepository(),
            authenticationBloc: AuthenticationBloc(
              AuthenticationRepository(),
            ),
          ),
        );
      case '/home':
        return MaterialPageRoute<void>(
          builder: (_) => const HomePage(),
        );
      //  case '/order':
      // return MaterialPageRoute<void>(
      //   builder: (_) => const OrderPage(),
      // );
      //  case '/product':
      // return MaterialPageRoute<void>(
      //   builder: (_) => const ProductPage(),
      // );
      default:
        return null;
      // default:
      //   return MaterialPageRoute<void>(
      //     builder: (_) => const PageNotFound(),
      //   );
    }
  }
}
