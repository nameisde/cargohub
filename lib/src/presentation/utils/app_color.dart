import 'package:flutter/material.dart';

const Color primaryColor = Color(0xff22A45D);
const Color darkGrayColor = Color(0xff303030);
const Color mediumGrayColor = Color(0xff868686);
const Color lightGrayColor = Color(0xffe5e5e5);
const Color veryLightGrayColor = Color(0xfff2f2f2);
const Color pendingColor = Color(0xffFACD39);
const Color failedColor = Color(0xffFA1716);
const Color whiteColor = Color(0xFFFFFFFF);
