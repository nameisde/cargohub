import 'package:flutter/material.dart';

const double marginTiny = 4;
const double marginSmall = 8;
const double marginRegular = 16;
const double marginMedium = 24;
const double marginLarge = 48;
const double marginSuperLarge = 96;

const double radiusTiny = 4;
const double radiusSmall = 8;
const double radiusRegular = 16;
const double radiusMedium = 24;
const double radiusLarge = 48;

const Widget horizontalSpaceTiny = SizedBox(width: 4);
const Widget horizontalSpaceSmall = SizedBox(width: 8);
const Widget horizontalSpaceRegular = SizedBox(width: 16);
const Widget horizontalSpaceMedium = SizedBox(width: 24);
const Widget horizontalSpaceLarge = SizedBox(width: 48);

const Widget verticalSpaceTiny = SizedBox(height: 4);
const Widget verticalSpaceSmall = SizedBox(height: 8);
const Widget verticalSpaceRegular = SizedBox(height: 16);
const Widget verticalSpaceMedium = SizedBox(height: 24);
const Widget verticalSpaceLarge = SizedBox(height: 48);
