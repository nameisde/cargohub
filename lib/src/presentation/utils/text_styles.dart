import 'package:flutter/material.dart';

const FontWeight light = FontWeight.w300;
const FontWeight regular = FontWeight.w400;
const FontWeight medium = FontWeight.w500;
const FontWeight semiBold = FontWeight.w600;
const FontWeight bold = FontWeight.w700;
const FontWeight extraBold = FontWeight.w800;
const FontWeight ultraBold = FontWeight.w900;

const TextStyle light300 = TextStyle(fontSize: 12, fontWeight: light);
const TextStyle light400 = TextStyle(fontSize: 12, fontWeight: regular);
const TextStyle light500 = TextStyle(fontSize: 12, fontWeight: medium);
const TextStyle light600 = TextStyle(fontSize: 12, fontWeight: semiBold);
const TextStyle light700 = TextStyle(fontSize: 12, fontWeight: bold);
const TextStyle light800 = TextStyle(fontSize: 12, fontWeight: extraBold);
const TextStyle light900 = TextStyle(fontSize: 12, fontWeight: ultraBold);

const TextStyle regular300 = TextStyle(fontSize: 14, fontWeight: light);
const TextStyle regular400 = TextStyle(fontSize: 14, fontWeight: regular);
const TextStyle regular500 = TextStyle(fontSize: 14, fontWeight: medium);
const TextStyle regular600 = TextStyle(fontSize: 14, fontWeight: semiBold);
const TextStyle regular700 = TextStyle(fontSize: 14, fontWeight: bold);
const TextStyle regular800 = TextStyle(fontSize: 14, fontWeight: extraBold);
const TextStyle regular900 = TextStyle(fontSize: 14, fontWeight: ultraBold);

const TextStyle medium300 = TextStyle(fontSize: 16, fontWeight: light);
const TextStyle medium400 = TextStyle(fontSize: 16, fontWeight: regular);
const TextStyle medium500 = TextStyle(fontSize: 16, fontWeight: medium);
const TextStyle medium600 = TextStyle(fontSize: 16, fontWeight: semiBold);
const TextStyle medium700 = TextStyle(fontSize: 16, fontWeight: bold);
const TextStyle medium800 = TextStyle(fontSize: 16, fontWeight: extraBold);
const TextStyle medium900 = TextStyle(fontSize: 16, fontWeight: ultraBold);

const TextStyle semiBold300 = TextStyle(fontSize: 18, fontWeight: light);
const TextStyle semiBold400 = TextStyle(fontSize: 18, fontWeight: regular);
const TextStyle semiBold500 = TextStyle(fontSize: 18, fontWeight: medium);
const TextStyle semiBold600 = TextStyle(fontSize: 18, fontWeight: semiBold);
const TextStyle semiBold700 = TextStyle(fontSize: 18, fontWeight: bold);
const TextStyle semiBold800 = TextStyle(fontSize: 18, fontWeight: extraBold);
const TextStyle semiBold900 = TextStyle(fontSize: 18, fontWeight: ultraBold);

const TextStyle bold300 = TextStyle(fontSize: 20, fontWeight: light);
const TextStyle bold400 = TextStyle(fontSize: 20, fontWeight: regular);
const TextStyle bold500 = TextStyle(fontSize: 20, fontWeight: medium);
const TextStyle bold600 = TextStyle(fontSize: 20, fontWeight: semiBold);
const TextStyle bold700 = TextStyle(fontSize: 20, fontWeight: bold);
const TextStyle bold800 = TextStyle(fontSize: 20, fontWeight: extraBold);
const TextStyle bold900 = TextStyle(fontSize: 20, fontWeight: ultraBold);

const TextStyle extraBold300 = TextStyle(fontSize: 28, fontWeight: light);
const TextStyle extraBold400 = TextStyle(fontSize: 28, fontWeight: regular);
const TextStyle extraBold500 = TextStyle(fontSize: 28, fontWeight: medium);
const TextStyle extraBold600 = TextStyle(fontSize: 28, fontWeight: semiBold);
const TextStyle extraBold700 = TextStyle(fontSize: 28, fontWeight: bold);
const TextStyle extraBold800 = TextStyle(fontSize: 28, fontWeight: extraBold);
const TextStyle extraBold900 = TextStyle(fontSize: 28, fontWeight: ultraBold);

const TextStyle ultraBold300 = TextStyle(fontSize: 36, fontWeight: light);
const TextStyle ultraBold400 = TextStyle(fontSize: 36, fontWeight: regular);
const TextStyle ultraBold500 = TextStyle(fontSize: 36, fontWeight: medium);
const TextStyle ultraBold600 = TextStyle(fontSize: 36, fontWeight: semiBold);
const TextStyle ultraBold700 = TextStyle(fontSize: 36, fontWeight: bold);
const TextStyle ultraBold800 = TextStyle(fontSize: 36, fontWeight: extraBold);
const TextStyle ultraBold900 = TextStyle(fontSize: 36, fontWeight: ultraBold);
