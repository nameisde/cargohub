part of 'authentication_bloc.dart';

abstract class AuthenticationEvent extends Equatable {
  const AuthenticationEvent();

  @override
  List<Object> get props => [];
}

class AppStarted extends AuthenticationEvent {
  @override
  String toString() => 'App Started';
}

class LogIn extends AuthenticationEvent {
  const LogIn({required this.token});
  final String token;

  @override
  List<Object> get props => [token];

  @override
  String toString() => 'LogIn {$token}';
}

class LogOut extends AuthenticationEvent {
  const LogOut({required this.token});
  final String token;

  @override
  List<Object> get props => [token];

  @override
  String toString() => 'LogOut {token: $token}';
}
