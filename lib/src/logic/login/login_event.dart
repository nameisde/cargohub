part of 'login_bloc.dart';

abstract class LoginEvent extends Equatable {
  const LoginEvent();

  @override
  List<Object> get props => [];
}

class UsernameChanged extends LoginEvent {
  const UsernameChanged({required this.username});

  final String username;

  @override
  List<Object> get props => [username];

  @override
  String toString() => 'UsernameChanged {username: $username}';
}

class PasswordChanged extends LoginEvent {
  const PasswordChanged({required this.password});

  final String password;

  @override
  List<Object> get props => [password];

  @override
  String toString() => 'PasswordChanged {password: $password}';
}

class Submitted extends LoginEvent {
  const Submitted({
    required this.username,
    required this.password,
  });

  final String username;
  final String password;

  @override
  List<Object> get props => [username, password];

  @override
  String toString() => 'Submitted{username: $username, password: $password}';
}
