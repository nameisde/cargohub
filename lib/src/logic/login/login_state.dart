part of 'login_bloc.dart';

class LoginState {
  LoginState({
    this.username = '',
    this.password = '',
  });

  final String username;

  final String password;

  LoginState copyWith({
    String? username,
    String? password,
  }) {
    return LoginState(
      username: username ?? this.username,
      password: password ?? this.password,
    );
  }
}

class LoginInitial extends LoginState {}

class LoginLoading extends LoginState {}

class LoginSubmiting extends LoginState {}

class LoginSuccess extends LoginState {}

class LoginFailure extends LoginState {
  LoginFailure({required this.error});
  final String error;

  List<Object> get props => [error];

  @override
  String toString() => 'LoginFailure { error: $error }';
}
