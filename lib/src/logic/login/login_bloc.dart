import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:cargohub/src/data/dio/exception.dart';
import 'package:cargohub/src/data/models/models.dart';
import 'package:cargohub/src/data/repository/authentication_repository.dart';
import 'package:cargohub/src/data/repository/login_xml_repository.dart';
import 'package:cargohub/src/logic/authentication/authentication_bloc.dart';
import 'package:dio/dio.dart';
import 'package:equatable/equatable.dart';
import 'package:simple_logger/simple_logger.dart';
import 'package:xml/xml.dart' as xml;

part 'login_event.dart';
part 'login_state.dart';

final logger = SimpleLogger();

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  LoginBloc({
    required this.authenticationRepository,
    required this.authenticationBloc,
  }) : super(LoginState());

  AuthenticationRepository authenticationRepository;
  AuthenticationBloc authenticationBloc;

  LoginState get initialState => LoginState();

  @override
  Stream<LoginState> mapEventToState(LoginEvent event) async* {
    if (event is UsernameChanged) {
      yield state.copyWith(username: event.username);
    } else if (event is PasswordChanged) {
      yield state.copyWith(password: event.password);
    } else if (event is Submitted) {
      yield LoginSubmiting();
      try {
        final LoginXMLRepository body = LoginXMLRepository(
          username: event.username,
          password: event.password,
        );

        logger.shout('RESPONSE BODY --> + $body');

        final response = await authenticationRepository.loginAPI(body);

        logger.shout('RESPONSE LOGIN API --> + $response');

        final parsedResponse = xml.XmlDocument.parse(response.toString());

        logger.shout('PARSED RESPONSE --> + $parsedResponse');

        final rows = parsedResponse
            .findAllElements('WindowTabData')
            .first
            .getAttribute('NumRows');

        logger.shout('ROWS + $rows');

        if (rows != '0') {
          yield LoginSuccess();
          authenticationBloc.add(LogIn(token: response.toString()));
        } else {
          yield LoginFailure(error: 'Username / Password Anda Salah');
        }
        // authenticationBloc.add(LogIn(token: response.toString()));
        // yield LoginSuccess();
      } on DioError catch (_) {
        final errorMessage = DioExceptions.fromDioError(_).toString();
        logger.shout(errorMessage);
        // yield LoginFailure(error: errorMessage);
      } on TimeoutException catch (_) {
        yield LoginFailure(error: 'Koneksi Anda bermasalah');
      } catch (e) {
        logger.shout(e);
        // yield LoginFailure(error: e.toString());
      }
    }
  }
}
