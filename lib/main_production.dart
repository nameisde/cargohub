import 'package:cargohub/app.dart';
import 'package:cargohub/src/data/repository/repository.dart';
import 'package:cargohub/src/logic/authentication/authentication.dart';
import 'package:cargohub/src/logic/observer/observer_bloc.dart';
import 'package:cargohub/src/presentation/router/app_router.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:hydrated_bloc/hydrated_bloc.dart';
import 'package:path_provider/path_provider.dart';

Future main() async {
  Bloc.observer = MyBlocObserver();
  WidgetsFlutterBinding.ensureInitialized();
  HydratedBloc.storage = await HydratedStorage.build(
    storageDirectory: await getApplicationDocumentsDirectory(),
  );
  await dotenv.load(fileName: 'assets/env/.env_production');
  runApp(App(
    appRouter: AppRouter(),
    connectivity: Connectivity(),
    authenticationRepository: AuthenticationRepository(),
    authenticationBloc: AuthenticationBloc(AuthenticationRepository()),
  ));
}
